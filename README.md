# Meteor Skeleton for @iDoMeteor

Meteor's create app is pretty handy, but once you settle in to a pattern,
it's nice to have your own skeleton that is a little less vanilla than
theirs.

Herein lies mine.

It includes the following:

Superfluous:
  * iDoMeteor-Coding-Guidelines.md - My personal (& opinionated) style guide for ECMA / Meteor development
  * .jscs-meteor-research.md - Very esoteric data regarding my creation of the .jscsrc for Meteor, best viewed raw, my markdown sucks. :)

Useful:
  * .csslint - For npm's csslint
  * .eslint - Based on node-style-guide & enhanced for Meteor compliance
  * .gitignore - Ignores vim swap files, .meteor/local, packages, private
  * .jscsrc - Super tuned for Meteor awesomeness, quickly fix your style
  * .meteor - A sane set of packages
  * .tern-project - For tern compliant editors, provides Meteor completions
  * README.md - This file! Should be edited for every project.
  * myproject - A dir to be renamed w/client/lib/server & some files
  * packages - A place to put your modularized local code
  * private - A place to hide your privates
  * public - A place to show your assetts
  * settings.json - A place for settings, should your project require them

There is a lot more information contained within the files themselves (the
project files, not so much yet).

Feel free to hit start up the issues or wiki, and I most certainly welcome
commentary via @iDoMeteor on Twitter or on http://forums.meteor.com.

Contributions in the form of pull requests are also welcome!
